#!/usr/bin/env python3

import discord
import requests
import datetime
import asyncio
import re
import yaml

class BotInfo:
    def load(self, file = "discoctf.yml"):
        with open(file) as f:
            config = yaml.safe_load(f)
        self.ctfd_token = config["ctfd_token"]
        self.ctfd_url = config["ctfd_url"]
        self.discord_token = config["discord_token"]
        self.discord_channel = config["discord_channel"]
        if "mindate" in config:
            self.mindate = config["mindate"]
        else:
            self.mindate = datetime.datetime.utcnow().isoformat()


    def save(self, file = "discoctf.yml"):
        config = {
                "ctfd_token": self.ctfd_token,
                "ctfd_url": self.ctfd_url,
                "discord_token": self.discord_token,
                "discord_channel": self.discord_channel,
                "mindate": self.mindate}
        with open(file, "w") as f:
            config = yaml.dump(config, f)


    def __init__(self, client):
        self.load()
        self.ctfd_session = requests.session()
        self.ctfd_session.headers['Authorization'] = "Token " + self.ctfd_token
        self.ctfd_session.headers['Content-Type'] = 'application/json'
        self.page = 1 # Current page on ctfd submissions

        self.client = client

    def get(self, path):
        res = self.ctfd_session.get(self.ctfd_url + path)
        try:
            j = res.json()
        except JSONDecodeError:
            print(f"{res} not JSON for GET {path}: {res.content}")
        if 'message' in j:
            print(f"Message: {j['message']} for GET {path}")
            return None
        return j


    async def check_submissions(self):
        "Vist all submissions and report new ones on discord"
        nextpage = self.page
        while nextpage:
            page = nextpage
            res = self.get(f"/api/v1/submissions?type=correct&page={page}")
            submissions = res['data']
            submissions.sort(key = lambda x: x['date'])
            for s in submissions:
                await self.check_submission(s)
            nextpage = res['meta']['pagination']['next']
        self.page = page
        self.save()


    async def check_submission(self,s):
        sub_date = s['date']
        user_id = s['user_id']
        user_name = s['user']['name']
        challenge_id = s['challenge_id']
        challenge_name = s['challenge']['name']
        challenge_value = s['challenge']['value']
        if sub_date <= self.mindate:
            #print(f"{sub_date} skip {user_name} pour {challenge_name}")
            return
        self.mindate = sub_date
        solves = self.get(f"/api/v1/challenges/{challenge_id}/solves")
        if not solves:
            return
        solves = solves['data']
        rank = None
        solves.sort(key = lambda x : x['date'], reverse=True)
        for i, solve in enumerate(reversed(solves)):
            if solve['account_id'] == user_id:
                rank = i + 1
                break
        if not rank:
            # Hidden solve (or error?)
            print(f"Hidden: {user_name} pour {challenge_name}")
            return
        dsname = dslink(user_name, f"{self.ctfd_url}/users/{user_id}")
        dschal = dslink(challenge_name, f"{self.ctfd_url}/challenges#{challenge_id}")
        if rank == 1:
            rankmsg = "le premier solve"
        else:
            rankmsg = f"le {rank}e solve"
        points = ""
        if challenge_value >= 2:
            points = "s"
        msg = f"Bravo à {dsname} pour {rankmsg} de {dschal} (+{challenge_value} point{points})"
        if rank == 1:
            msg = ":fire: " + msg
        print(f"{sub_date}:{msg}")
        await self.client.flag_chan.send(msg)



def dslink(text, url):
    "generate a valid and robust link for discord or try to, this especially takes care of emojis"

    # Fixme: [ ] ( ) in text

    match = re.search("[\w -~]+", text)
    res = ""
    if match.start() > 0:
        res += text[0:match.start()-1]
    res += "[" + match[0] + "](" + url.replace(")","\)") + ")" + text[match.end():]
    return res


class MyClient(discord.Client):

    async def on_ready(self):
        print(f'Logged on as {self.user}')
        self.flag_chan = self.get_channel(info.discord_channel)
        print(f'Guilde: {self.flag_chan.guild.name}')
        print(f'Channel: {self.flag_chan.name}')
        print(f"Topic: {self.flag_chan.topic}")

        while True:
            await info.check_submissions()
            await asyncio.sleep(10)


intents = discord.Intents.default()
client = MyClient(intents=intents)
info = BotInfo(client)
client.run(info.discord_token)
